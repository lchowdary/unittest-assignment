package priya.example.unittesting;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Creation of a class for performing SQL Operations from JDBC
 * 
 * @author lchowdary
 *
 */
public class JDBC {
	static final String DB_URL = "jdbc:mysql://localhost/priya";
	static final String USER = "root";
	static final String PASS = "root";
	
	 public void viewDataBases() {
	        Statement stmt = null;
	        ResultSet resultset = null;
	        try {
	            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
	            stmt = conn.createStatement();
	            resultset = stmt.executeQuery("SHOW DATABASES;");
	            if (stmt.execute("SHOW DATABASES;")) {
	                resultset = stmt.getResultSet();
	            }
	            while (resultset.next()) {
	                System.out.println(resultset.getString("Database"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	 
	 public void countTables() {
	        Statement stmt = null;
	        ResultSet resultset = null;
	        try {
	            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
	            stmt = conn.createStatement();
	              String query = "select count(*) as  totalnumberoftables from information_schema.tables where table_schema='priya'";
	              
	              ResultSet rs = stmt.executeQuery(query);
	              rs.next();
	              int count = rs.getInt(1);
	              System.out.println("Number of records in the employee table: "+count);
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	 
	 public static void main(String args[]) {
		    JDBC  obj = new JDBC ();
		    obj.viewDataBases();
		    obj.countTables();
		}




}
