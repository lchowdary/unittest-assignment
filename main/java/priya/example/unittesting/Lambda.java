package priya.example.unittesting;

public class Lambda {
	public int Add(int a, int b) {
		return a + b;
	}

	@FunctionalInterface
	public
	interface Adding {
		int add(int a, int b);
	}

   public static Adding operation = (a, b) -> a + b;
//		System.out.println(ad.add(50,100));
}
