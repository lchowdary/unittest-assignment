package priya;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import priya.example.unittesting.Daemon_threads;

class Daemon_test {

	@Test
	void test() {
		
		Daemon_threads t1 = new Daemon_threads();
		t1.setDaemon(true);
		assertEquals(false,Daemon_threads.currentThread().isDaemon());
	    assertEquals(true, t1.isDaemon());


		
	}

}
