package priya;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import priya.example.unittesting.Question_7;

public class Question_7_test {

	@Test
	
		
		public void Differenceindates()
	    {
	        Question_7 main = new Question_7();
	        LocalDate date1 = LocalDate.of(2022,12,31);
	        LocalDate date2 = LocalDate.of(2021,11,30);
	        LocalDate ExpectedLocalDate = LocalDate.of(1,1,1);
	        LocalDate ActualLocalDate = main.difference_in_dates(date1,date2);
	        Assertions.assertEquals(ExpectedLocalDate,ActualLocalDate);
	        
	        
	    }
		
	}


