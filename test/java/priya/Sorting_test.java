package priya;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import priya.example.unittesting.Sorting_employees;

public class Sorting_test {
	

	@Test
	public void test() {
		
		
		List<Sorting_employees> employeeslist = new ArrayList<Sorting_employees>();
	      employeeslist.add(new Sorting_employees("Snehith", 22, 20000)); 
	      employeeslist.add(new Sorting_employees("Shree", 19, 35000)); 
	      employeeslist.add(new Sorting_employees("Manu", 23, 30000)); 
	      employeeslist.add(new Sorting_employees("Priya", 20, 50000)); 
	      employeeslist.add(new Sorting_employees("Lucky", 18, 40000));
	      System.out.println("Before Sorting the Employees data:"); 
	 
	      //forEach for printing the list 
	      employeeslist.forEach((s)->System.out.println(s));

	      
	      //forEach for printing the list
	      employeeslist.forEach((s)->System.out.println(s));         

	      System.out.println("After Sorting the employees data by Name:"); 
	      //Lambda expression for sorting the list by employee name       
	      employeeslist.sort((Sorting_employees e1, Sorting_employees e2)->e1.getName().compareTo(e2.getName())); 
	      employeeslist.forEach((s)->System.out.println(s)); 
		
	}

}
