package priya;

import static org.junit.Assert.*;

import org.junit.Test;

import priya.example.unittesting.Unittest_example;

public class Testclass {

	@Test
	public void non_staticcount() {
		Unittest_example tests = new Unittest_example();
		Unittest_example tests1 = new Unittest_example();
		System.out.println(tests1.non_staticcount);
		assertNotEquals(3, tests1.non_staticcount);
	}

	@Test
	public void countstatic() {
		Unittest_example test = new Unittest_example();
		System.out.println(test.countstatic);
		Unittest_example test1 = new Unittest_example();
		System.out.println(test1.countstatic);
		Unittest_example test2 = new Unittest_example();
		System.out.println(test.countstatic);
		assertEquals(3,test.countstatic);

	}

}
